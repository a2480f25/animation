//
//  ViewController.m
//  animation
//
//  Created by Stefan Szekeres on 16/10/14.
//  Copyright (c) 2014 Stefan Szekeres. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>
void ToneInterruptionListener(void *inClientData, UInt32 inInterruptionState)
{
    ViewController *viewController =
    (__bridge ViewController *)inClientData;
    
    //[viewController stop];
}
OSStatus RenderTone(
                    void *inRefCon,
                    AudioUnitRenderActionFlags 	*ioActionFlags,
                    const AudioTimeStamp 		*inTimeStamp,
                    UInt32 						inBusNumber,
                    UInt32 						inNumberFrames,
                    AudioBufferList 			*ioData)

{
    // Fixed amplitude is good enough for our purposes
    const double amplitude = 1;//0.25;
    
    
    
    // Get the tone parameters out of the view controller
    ViewController *viewController =
    (__bridge ViewController *)inRefCon;
    double theta = viewController->theta;
    double theta2 = viewController->theta2;
    double theta3 = viewController->theta3;
    
    
    //NSLog(@"%f",theta);
    double theta_increment = 2.0 * M_PI * viewController->frequency / viewController->sampleRate;
    double theta_increment2 = 2.0 * M_PI * viewController->frequency2 / viewController->sampleRate;
    double theta_increment3 = 2.0 * M_PI * viewController->frequency3 / viewController->sampleRate;
    
    
    // This is a mono tone generator so we only need the first buffer
    const int channel = 0;
    Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
    //    Float32 *tmp1 =(Float32 *)ioData->mBuffers[channel].mData;
    //    Float32 *tmp2 = (Float32 *)ioData->mBuffers[channel].mData;
    //    for (UInt32 frame = 0; frame < inNumberFrames; frame++)
    //    {
    //        tmp1[frame] = sin(theta) * amplitude;
    //        tmp2[frame] = sin(theta) * (amplitude);
    //        theta += theta_increment;
    //        if (theta > 2.0 * M_PI)
    //        {
    //            theta -= 2.0 * M_PI;
    //        }
    //    }
    //    for (UInt32 frame = 0; frame < inNumberFrames; frame++) {
    //        buffer[frame] = amplitude * (tmp1[frame] + tmp2[frame]) / 2.0;
    //    }
    //NSLog(@"%f",*buffer);
    // Generate the samples
    //NSLog(@"%u",(unsigned int)inNumberFrames);
    //------------------
    for (UInt32 frame = 0; frame < inNumberFrames; frame++)
    {
        if (frame > 0 && frame < 20) {
            buffer[frame] = sin(theta) * amplitude;
        }
        else if (frame > 100 && frame <120) {
            buffer[frame] = sin(theta2) * amplitude;
        }
        else if (frame > 500 && frame <520) {
            buffer[frame] = sin(theta3) * amplitude;
        }
        else
            buffer[frame] = 0;
        theta += theta_increment;
        theta2 += theta_increment2;
        theta3 += theta_increment3;
        
        if (theta > 2.0 * M_PI)
        {
            theta -= 2.0 * M_PI;
        }
        if (theta2 > 2.0 * M_PI)
        {
            theta2 -= 2.0 * M_PI;
        }
        if (theta3 > 2.0 * M_PI)
        {
            theta3 -= 2.0 * M_PI;
        }
        
    }
    
    
    
    //-------------------------
    //
    //    for (UInt32 frame = 0; frame < 256; frame++)
    //    {
    //        buffer[frame] = sin(theta) * amplitude;
    //
    //        theta += theta_increment;
    //        if (theta > 2.0 * M_PI)
    //        {
    //            theta -= 2.0 * M_PI;
    //        }
    //    }
    //    for (UInt32 frame = 257; frame < inNumberFrames; frame++)
    //    {
    //        buffer[frame] = 0;
    //
    //        theta += theta_increment;
    //        if (theta > 2.0 * M_PI)
    //        {
    //            theta -= 2.0 * M_PI;
    //        }
    //    }
    
    // Store the theta back in the view controller
    viewController->theta = theta;
    viewController->theta2 = theta2;
    viewController->theta3 = theta3;
    
    return noErr;
}
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *iV;

@end

@implementation ViewController

//- (void) viewWillDisappear:(BOOL)animated {
//    if (toneUnit)
//    {
//            AudioOutputUnitStop(toneUnit);
//            AudioUnitUninitialize(toneUnit);
//            AudioComponentInstanceDispose(toneUnit);
//            toneUnit = nil;
//            
//        }
//}
//
//- (void) applicationDidEnterBackground:(UIApplication *)application {
//    if (toneUnit)
//    {
//        AudioOutputUnitStop(toneUnit);
//        AudioUnitUninitialize(toneUnit);
//        AudioComponentInstanceDispose(toneUnit);
//        toneUnit = nil;
//    }
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    frequency = 440;
    frequency2 = 600;
    frequency3 = 800;
    sampleRate = 44100;
    
    OSStatus result = AudioSessionInitialize(NULL, NULL, ToneInterruptionListener, (__bridge void *)(self));
    if (result == kAudioSessionNoError)
    {
        UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
        
        //float aBufferLength = 0.001; // In seconds
        AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
        //AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(aBufferLength), &aBufferLength);
        
    }
    AudioSessionSetActive(true);
    
    [self createToneUnit];
    OSErr err = AudioUnitInitialize(toneUnit);
    NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
    
    // Start playback
    err = AudioOutputUnitStart(toneUnit);
    NSAssert1(err == noErr, @"Error starting unit: %hd", err);
    
    NSMutableArray* images = [NSMutableArray array];
    for (int i = 0;i < 25; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"test%d_y.jpg",i]]];
    }
    
    _iV.animationImages = images;
    _iV.animationDuration = 0.20;
    
    [_iV startAnimating];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)createToneUnit
{
    // Configure the search parameters to find the default playback output unit
    // (called the kAudioUnitSubType_RemoteIO on iOS but
    // kAudioUnitSubType_DefaultOutput on Mac OS X)
    AudioComponentDescription defaultOutputDescription;
    defaultOutputDescription.componentType = kAudioUnitType_Output;
    defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    defaultOutputDescription.componentFlags = 0;
    defaultOutputDescription.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
    NSAssert(defaultOutput, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
    NSAssert1(toneUnit, @"Error creating unit: %hd", err);
    
    // Set our tone rendering function on the unit
    AURenderCallbackStruct input;
    input.inputProc = RenderTone;
    input.inputProcRefCon = (__bridge void *)(self);
    
    err = AudioUnitSetProperty(toneUnit,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &input,
                               sizeof(input));
    NSAssert1(err == noErr, @"Error setting callback: %hd", err);
    
    // Set the format to 32 bit, single channel, floating point, linear PCM
    const int four_bytes_per_float = 4;
    const int eight_bits_per_byte = 8;
    AudioStreamBasicDescription streamFormat;
    streamFormat.mSampleRate = sampleRate;
    streamFormat.mFormatID = kAudioFormatLinearPCM;
    streamFormat.mFormatFlags = kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
    streamFormat.mBytesPerPacket = four_bytes_per_float;
    streamFormat.mFramesPerPacket = 1;
    streamFormat.mBytesPerFrame = four_bytes_per_float;
    streamFormat.mChannelsPerFrame = 1;
    streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
    err = AudioUnitSetProperty (toneUnit,
                                kAudioUnitProperty_StreamFormat,
                                kAudioUnitScope_Input,
                                0,
                                &streamFormat,
                                sizeof(AudioStreamBasicDescription));
    NSAssert1(err == noErr, @"Error setting stream format: %hd", err);
}

- (void) getMatch {
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

}

- (void) getMatchListWS {
    // Stop changing parameters on the unit
    OSErr err = AudioUnitInitialize(toneUnit);
    NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
    
    // Start playback
    err = AudioOutputUnitStart(toneUnit);
    
}
- (void) getMatchListWS2 {
    AudioOutputUnitStop(toneUnit);
    AudioUnitUninitialize(toneUnit);
    AudioComponentInstanceDispose(toneUnit);
    toneUnit = nil;
    
}


@end
