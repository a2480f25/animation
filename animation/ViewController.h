//
//  ViewController.h
//  animation
//
//  Created by Stefan Szekeres on 16/10/14.
//  Copyright (c) 2014 Stefan Szekeres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioUnit/AudioUnit.h>

@interface ViewController : UIViewController <UIApplicationDelegate> {
    UILabel *frequencyLabel;
    UIButton *playButton;
    UISlider *frequencySlider;
    AudioComponentInstance toneUnit;
    
@public
    double frequency;
    double frequency2;
    double frequency3;
    double sampleRate;
    double theta;
    double theta2;
    double theta3;
}

@end

